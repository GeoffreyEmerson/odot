# README #

This repository is for my personal effort to follow the Treehouse course "Build a Todo List Application with Rails 4", and continuing on with "User Authentication with Rails".

### What is this repository for? ###

* Offsite backup
* Practice with git
* Sharing code when problems occur

### How do I get set up? ###

An effort was made to match the setup of the instructor in a local development environment: Ubuntu 14 on VirtualBox.
* Ruby version is 2.0.0p647
* Rails is 4.0.1

Unfortunately some errors came up for unknown reasons, and a couple gems were updated in an attempt to compensate.
* See Gemfile

### Who do I talk to? ###

* Bitbucket user GeoffreyEmerson
* Teamtreehouse user geoffreyemerson
