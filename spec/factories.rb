FactoryGirl.define do
  factory :user do
    first_name            "Factory"
    last_name             "User"
    sequence(:email)      { |n| "user#{n}@example.com" }
    password              "treehouse1"
    password_confirmation "treehouse1"
  end

  factory :todo_list do
    title "Todo List Factory Title"
    description "Todo list factory description."
    user
  end
end
