require 'spec_helper'

describe "Adding todo items" do
  let(:user) { todo_list.user } # Not created until called
  let!(:todo_list) { create(:todo_list) } # This is calling Factory Girl
  before { sign_in user, password: "treehouse1" }

  it "is successful with vaild content" do
    visit_todo_list(todo_list)
    click_link "New Todo Item"
    fill_in "Content", with: "Milk"
    click_button "Save"
    expect(page).to have_content("Added todo list item.")
    within("table.todo_items") do
      expect(page).to have_content("Milk")
    end
  end

  it "displays an error when there is no content" do
    visit_todo_list(todo_list)
    click_link "New Todo Item"
    fill_in "Content", with: ""
    click_button "Save"
    within("div.flash") do
      expect(page).to have_content("There was a problem adding that todo list item.")
    end
    expect(page).to have_content("Content can't be blank")
  end

  it "displays an error when the content is too short" do
    visit_todo_list(todo_list)
    click_link "New Todo Item"
    fill_in "Content", with: "1"
    click_button "Save"
    within("div.flash") do
      expect(page).to have_content("There was a problem adding that todo list item.")
    end
    expect(page).to have_content("Content is too short")
  end
end
