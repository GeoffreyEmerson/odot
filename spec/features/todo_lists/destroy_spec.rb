require 'spec_helper'

describe "Deleting todo lists" do
  let(:user) { todo_list.user } # Not created until called
  let!(:todo_list) { create(:todo_list) } # This is calling Factory Girl

  # This makes sure we have a test user that is logged in for the other tests to work.
  before do
    sign_in user, password: "treehouse1"
  end

  it "is successful when clicking the destroy link" do
    visit "/todo_lists"

    within "#todo_list_#{todo_list.id}" do
      click_link "Destroy"
    end
    expect(page).to_not have_content(todo_list.title)
    expect(TodoList.count).to eq(0)

  end

end
