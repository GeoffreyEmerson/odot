require 'spec_helper'

describe "Editing todo lists" do

  let(:user) { todo_list.user } # Not created until called
  let!(:todo_list) { create(:todo_list) } # This is calling Factory Girl

  # This is a function that will be used below during tests.
  def update_todo_list(options={})
    options[:title] ||= "My todo list"
    options[:description] ||= "This is my todo list."
    todo_list = options[:todo_list]

    visit "/todo_lists"
    within "#todo_list_#{todo_list.id}" do
      click_link "Edit"
    end

    fill_in "Title", with: options[:title]
    fill_in "Description", with: options[:description]
    click_button "Update Todo list"
  end

  # This makes sure we have a test user that is logged in for the other tests to work.
  before do
    sign_in todo_list.user, password: "treehouse1"
  end

  it "updates todo list when new information is submitted" do
    update_todo_list todo_list: todo_list,
                     title: "New title",
                     description: "New description."

    todo_list.reload

    expect(page).to have_content("Todo list was successfully updated.")
    expect(todo_list.title).to eq("New title")
    expect(todo_list.description).to eq("New description.")
  end

  it "displays an error when no title is given" do
    update_todo_list todo_list: todo_list, title: ""
    title = todo_list.title
    todo_list.reload
    expect(todo_list.title).to eq(title)
    expect(page).to have_content("error")
  end

  it "display an error when the title is too short" do
    update_todo_list todo_list: todo_list, title: "Hi"
    title = todo_list.title
    todo_list.reload
    expect(todo_list.title).to eq(title)
    expect(page).to have_content("error")
  end

  it "displays an error when no description is given" do
    update_todo_list todo_list: todo_list, description: ""
    description = todo_list.description
    todo_list.reload
    expect(todo_list.description).to eq(description)
    expect(page).to have_content("error")
  end

  it "displays an error when the description is too short" do
    update_todo_list todo_list: todo_list, description: "hi"
    description = todo_list.description
    todo_list.reload
    expect(todo_list.description).to eq(description)
    expect(page).to have_content("error")
  end

end
